﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ElevatorCtrl : MonoBehaviour {

    private Transform tr;
    private Transform playerTr;

    private bool isUp = true;
    private bool isDown = false;

    public Animation ani;

    // Use this for initialization
    void Start () {
        tr = GetComponent<Transform>();
        playerTr = GetComponent<Transform>();

        ani = GetComponentInChildren<Animation>();
    }
	
	// Update is called once per frame
	void Update () {

    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            if (tr.position.y < 15.0f)
            {
                isUp = true;
                ani.Play("Lift");
                Debug.Log("isUp = " + isUp.ToString());
            }
            else if (tr.position.y >= 15.0f)
            {
                isDown = true;
                ani.Play("LiftDown");
                Debug.Log("isDown = " + isDown.ToString());
            }
        }
    }
}
