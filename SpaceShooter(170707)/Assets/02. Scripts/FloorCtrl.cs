﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorCtrl : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision coll)
    {

        if(coll.collider.tag == "Missile")
        {
            Collider[] colls = Physics.OverlapSphere(coll.transform.position, 100.0f);
            foreach (Collider Coll in colls)
            {
                Rigidbody rbody = Coll.GetComponent<Rigidbody>();
                if (rbody != null)
                {
                    rbody.AddExplosionForce(20.0f, coll.transform.position, 100.0f);
                }
            }
            Destroy(coll.gameObject);
        }
    }
}
