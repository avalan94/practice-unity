﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class FireCtrl : MonoBehaviour {

    public GameObject bullet;
    public GameObject missile;
    public Transform firePos;
    public Transform UltimatePos;

    public MeshRenderer muzzleFlash;

    public Image skillImage;
    public Text skillAmountText;
    private bool canUseSkill = true;
    public float coolTime;
    //private float currentCoolTime;

    public Image reloadImage;
    public Text reloadText;
    private bool canReload = true;
    private bool onReload = false;
    public float reloadCoolTime;

    public int bulletCountMax;
    public int bulletCount;

    private GameUI gameUI;

    private UIMgr UIDATA;

    public AudioClip fireSound;

    void Start()
    {
        muzzleFlash.enabled = false;

        skillImage.fillAmount = 0;
        reloadImage.fillAmount = 0;

        gameUI = GameObject.Find("GameUI").GetComponent<GameUI>();

        skillAmountText.text = "1";
        reloadText.text = "<color=#00FF00FF>Reload</color>";

        UIDATA = GameObject.Find("UIManager").GetComponent<UIMgr>();
        bulletCountMax = UIDATA.magazine;
        bulletCount = bulletCountMax;
    }

    // Update is called once per frame
    

	// Update is called once per frame
	void Update () {
        if(!gameUI.isPause)
        {
            if (Input.GetMouseButtonDown(0))
            {
                //Fire();
            }
            else if (Input.GetMouseButtonDown(1))
            {
                //UseSkill();
            }
            else if (Input.GetKeyDown(KeyCode.R))
            {
                bulletCount = bulletCountMax;
                gameUI.DispBulletCount(bulletCountMax);
                Debug.Log("Reloaded");
            }
        }
	}

    public void Fire()
    {
        if(bulletCount > 0 && !onReload)
        {
            CreateBullet();
            AudioSource.PlayClipAtPoint(fireSound, firePos.position);
            StartCoroutine(ShowMuzzleFlash());
        }
    }

    public void UseSkill()
    {
        if (canUseSkill)
        {
            skillImage.fillAmount = 1;
            StartCoroutine("CoolTime");

            //currentCoolTime = coolTime;
            skillAmountText.text = "0";

            canUseSkill = false;

            CreateMissile();
        }
        else
        {
            //사용불가
        }
    }

    public void Reload()
    {
        if (canReload)
        {
            onReload = true;
            reloadImage.fillAmount = 1;
            StartCoroutine("ReloadTime");

            reloadText.text = "<color=#5A5A5AFF>Reload</color>";

            canReload = false;
        }
        else
        {
            //사용불가
        }
    }

    void CreateBullet()
    {
        Instantiate(bullet, firePos.position, firePos.rotation);
        bulletCount--;
        gameUI.DispBulletCount(bulletCount);
    }

    void CreateMissile()
    {
        Instantiate(missile, UltimatePos.position, UltimatePos.rotation);
    }

    IEnumerator ShowMuzzleFlash()
    {
        float scale = Random.Range(1.0f, 2.0f);
        muzzleFlash.transform.localScale = Vector3.one * scale;

        Quaternion rot = Quaternion.Euler(0, 0, Random.Range(0, 360));
        muzzleFlash.transform.localRotation = rot;

        muzzleFlash.enabled = true;

        yield return new WaitForSeconds(Random.Range(0.05f, 0.3f));

        muzzleFlash.enabled = false;
    }

    IEnumerator CoolTime()
    {
        while (skillImage.fillAmount > 0)
        {
            skillImage.fillAmount -= 1 * Time.smoothDeltaTime / coolTime;
            yield return null;
        }

        skillAmountText.text = "1";
        canUseSkill = true;
        yield break;
    }

    //IEnumerator CoolTimeCounter()
    //{
    //    //쿨타임 시간 텍스트 표시용
    //    while (currentCoolTime > 0)
    //    {
    //        yield return new WaitForSeconds(1.0f);

    //        currentCoolTime -= 1.0f;
    //    }
    //    yield break;
    //}

    IEnumerator ReloadTime()
    {
        while (reloadImage.fillAmount > 0)
        {
            reloadImage.fillAmount -= 1 * Time.smoothDeltaTime / reloadCoolTime;
            yield return null;
        }

        bulletCount = bulletCountMax;
        gameUI.DispBulletCount(bulletCountMax);
        onReload = false;

        reloadText.text = "<color=#00FF00FF>Reload</color>";
        canReload = true;
        yield break;
    }

}
