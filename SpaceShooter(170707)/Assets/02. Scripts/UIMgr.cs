﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMgr : MonoBehaviour {

    private int num;

    public int attack = 0;          //max == 20
    public float speed = 0.0f;      //max == 10.0f
    public float distance = 0.0f;   //max == 50.0f
    public int magazine = 0;        //max == 20

    public Button Btn01;
    public Button Btn02;
    public Button Btn03;
    public Button BtnMin01;
    public Button BtnMin02;
    public Button BtnMin03;

    public GameObject player;

    //private Touch tempTouchs;
    //private Vector3 touchedPos;
    //private bool touchOn;

    public void Start()
    {
        DontDestroyOnLoad(this);

        num = 1;
        attack = 10;
        speed = 5.0f;
        distance = 30.0f;
        magazine = 10;
    }

    public void Update()
    {
        //if(Input.touchCount > 0)
        //{
        //    for(int i = 0; i < Input.touchCount; i++)
        //    {
        //        tempTouchs = Input.GetTouch(i);
        //        if(tempTouchs.phase == TouchPhase.Began)
        //        {
        //            touchedPos = Camera.main.ScreenToWorldPoint(tempTouchs.position);
        //            touchOn = true;

        //            break;
        //        }
        //    }
        //}
    }

    public void onClickStartBtn()
    {
        Application.LoadLevel("scLevel01");
        Application.LoadLevelAdditive("scPlay");
    }

    public void onClickExitBtn()
    {
        Application.Quit();
    }

    public void onClickOption()
    {

    }

    public void onClickCharacter01()
    {
        num = 1;
        attack = 10;
        speed = 5.0f;
        distance = 30.0f;
        magazine = 10;

        player.transform.localPosition = new Vector3(-70.0f, -19.0f, -500.0f);

        Btn01.transform.localPosition = new Vector3(-350.0f, 0, 0);
        BtnMin02.transform.localPosition = new Vector3(-110.0f, 0, 0);
        Btn02.transform.localPosition = new Vector3(-1300.0f, 0, 0);
        BtnMin03.transform.localPosition = new Vector3(-40.0f, 0, 0);
        Btn03.transform.localPosition = new Vector3(-1300.0f, 0, 0);
    }
    public void onClickCharacter02()
    {
        num = 2;
        attack = 15;
        speed = 7.5f;
        distance = 40.0f;
        magazine = 15;

        player.transform.localPosition = new Vector3(-50.0f, -19.0f, -500.0f);

        Btn01.transform.localPosition = new Vector3(-1300.0f, 0, 0);
        BtnMin02.transform.localPosition = new Vector3(-450.0f, 0, 0);
        Btn02.transform.localPosition = new Vector3(-280.0f, 0, 0);
        BtnMin03.transform.localPosition = new Vector3(-40.0f, 0, 0);
        Btn03.transform.localPosition = new Vector3(-1300.0f, 0, 0);
    }
    public void onClickCharacter03()
    {
        num = 3;
        attack = 20;
        speed = 10.0f;
        distance = 50.0f;
        magazine = 20;

        player.transform.localPosition = new Vector3(-35.0f, -19.0f, -500.0f);

        Btn01.transform.localPosition = new Vector3(-1300.0f, 0, 0);
        BtnMin02.transform.localPosition = new Vector3(-450.0f, 0, 0);
        Btn02.transform.localPosition = new Vector3(-1300.0f, 0, 0);
        BtnMin03.transform.localPosition = new Vector3(-380.0f, 0, 0);
        Btn03.transform.localPosition = new Vector3(-210.0f, 0, 0);
    }

}
